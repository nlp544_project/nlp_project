import json
import re
import os
import string
import urllib.request
from bs4 import BeautifulSoup
from datetime import datetime, date, timedelta

BASE_URL = "http://techcrunch.com/"

def get_soup( url ):
    try:
        response = urllib.request.urlopen( url )
    except:
        print ("ERROR GETTING URL: ", url)
        return None
    the_page = response.read()
    soup = BeautifulSoup( the_page )
    return soup

def get_article_links(soup):
    article_links = []
    for x in soup.findAll("li", {"class" : "river-block"}):
        article_meta = {}
        article_meta["data-permalink"] = x.get("data-permalink")
        article_meta["header"] = x.get("data-sharetitle").replace(u'\xa0', u' ')
        article_links.append(article_meta)
    return article_links
        

def get_content(content_date):
    url = BASE_URL + content_date.strftime("%Y/%m/%d")
    links = []
    for page in range(1,100):
        url2 = url + "/page/" + str(page)
        soup = get_soup(url2)
        if soup is None:
            return links
        # If page is not not found, return
        page_title = soup.find("div", {"class" : "page-title"})
        if page_title is not None and page_title.find("h1") is not None:
             if page_title.find("h1").text == "Page Not Found":
                 return links
        page_links = get_article_links(soup)
        if len(page_links) > 0:
            links.extend(page_links)
        else:
            print ("No links in " + url2)
            return links

def save_to_file(article_date, day_count, url, title, tags,
                     author, body):
    dir = "./techcrunch_files"
    if not os.path.exists(dir):
        os.makedirs(dir)
    date = article_date.strftime("%Y%m%d")
    file_op = open(dir + "/" + str(date) + "_" + str(day_count),"w", errors="ignore")
    file_op.write("URL: " + url + "\n")
    file_op.write("TITLE: " + title + "\n")
    file_op.write("TAGS: " + ",".join(tags) + "\n")
    file_op.write("AUTHOR: " + author + "\n")
    file_op.write(body + "\n")
    file_op.close()

def get_tags(soup):
    tags = []
    for x in soup.findAll("div", {"class":"tag-item"}):
        try:
            tags.append(x.find("a", {"class":"tag"}).text.strip())
        except:
            pass
    return tags

def get_author(url, soup):
    author = ""
    try:
        author = soup.find("div", {"class" : "byline"}).find("a").get("href")
        return author
    except:
        try:
            for x in soup.findAll("div", {"class" : "byline"}):
                if x.find("a") is not None:
                    return x.find("a").get("href")
        except:
            try:
                author = soup.find("div", {"class": "title-left"}).find("a").get("href")
                return author
            except:
                return author

def get_article_content(soup):
    content_dom = soup.find("div", {"class":"article-entry"})
    body = ""
    for c in content_dom.findAll("p"):
        body += c.text + "\n"
    #print (body)
    return body

def get_article_contents(links, article_date):
    day_count = 0
    for l in links:
        try:
            day_count += 1
            url = l["data-permalink"]
            #print (url)
            article_soup = get_soup(url)
            title = article_soup.find("h1", {"class":"alpha tweet-title"}).text
            #print (title)
            tags = get_tags(article_soup)
            #print (tags)
            #x = article_soup.find("div", {"class": "title-left"})
            #date = x.find("time").get("datetime")
            author= get_author(url, article_soup)
            if author is None or author == "":
                author = ""
                print ("Author not found for: " + url)
            body = get_article_content(article_soup)
            save_to_file(article_date, day_count, url, title, tags,
                         author, body)
            if day_count % 20 == 0:
                print ("Done with " + str(day_count))
        except:
            print ("Error extracting from: " + url)
        

def main(s_y, s_m, s_d, e_y, e_m, e_d):
    start_date = date(s_y, s_m, s_d)
    end_date = date(e_y, e_m, e_d)
    d1 = start_date
    total_count = 0
    while(d1 <= end_date):
        try:
            all_links = []
            all_links.extend(get_content(d1))
            d1_str = d1.strftime("%Y/%m/%d")
            print ("Links found for " + str(d1_str) + ": " + str(len(all_links)))
            get_article_contents(all_links, d1)
            d1= d1 + timedelta(days = 1)
            total_count += len(all_links)
        except:
            pass
    print ("Downloaded " + str(total_count) + " documents")
    
    
if __name__ == "__main__":
    main(2014,1,1, 2015,4,11)
