'''
Author: Anvesha Sinha
Date: 04/12/2015

This script processes the scrapped articles from Techcrunch and cleans them so that the files can be fed into Mallet.
'''

from nltk.stem.snowball import *
from nltk.stem.wordnet import WordNetLemmatizer
import sys
from os import listdir
import os
import string
import codecs
import shutil
import nltk

#This function stems files that contain relevant words without punctuations
def stemFiles(DocDir):
	stemmer = SnowballStemmer("english")
	NewDir = "Stemmed_data/"
	if not os.path.exists(NewDir):
		os.makedirs(NewDir)
	
	for document in sorted(listdir(DocDir)):
		print (DocDir+document)
		openFile = codecs.open(DocDir+document, "r", encoding="utf-8")
		writeFile = codecs.open(NewDir+document, "a", encoding="utf-8")
		lines = openFile.readlines()
		
		for line in lines:
		    newLine = ""
		    wordList = line.split(" ")
		    for word in wordList:
		        newLine += " " + stemmer.stem(word)
		    
		    writeFile.write(newLine)


#This function is for stemming the query entered by the user
def stemQuery(query):
	stemmer = SnowballStemmer("english")
	query = getOnlyNouns(query)
	wordList = query.split(" ")

	newQueryList = []
	for word in wordList:
		newQueryList.append(stemmer.stem(word))

	return " ".join(newQueryList)

def lemmatizeFiles(DocDir):
	lemmatizer = WordNetLemmatizer()
	NewDir = "Lemmatized_data/"
    
	if not os.path.exists(NewDir):
		os.makedirs(NewDir)
	
	for document in sorted(listdir(DocDir)):
		print (DocDir+document)
		openFile = codecs.open(DocDir+document, "r", encoding="utf-8")
		writeFile = codecs.open(NewDir+document, "a", encoding="utf-8")
		lines = openFile.readlines()
		
		for line in lines:
		    newLine = ""
		    wordList = line.split(" ")
		    for word in wordList:
		        newLine += " " + lemmatizer.lemmatize(word)
		    
		    writeFile.write(newLine)

def lemmatizeQuery(query):
	lemmatizer = WordNetLemmatizer()
	query = getOnlyNouns(query)
	wordList = query.split(" ")
    
	newQueryList = []
	for word in wordList:
		newQueryList.append(lemmatizer.lemmatize(word))

	return " ".join(newQueryList)

#This function removes all the unnecessary words from the files before stemming/lemmatizing. It creates a new folder with files that don't have punctuations and other relevant words (URLs removed)
def cleanFiles(DocDir):
    exclude = set(string.punctuation)
    
    if not os.path.exists("Cleaned_data/"):
        os.makedirs("Cleaned_data/")
    
    NewDir = "Cleaned_data/"
    for document in sorted(listdir(DocDir)):
        print (DocDir+document)
        openFile = codecs.open(DocDir+document, "r", encoding="utf-8", errors="ignore")
        writeFile = codecs.open(NewDir+document, "a", encoding="utf-8", errors="ignore")
        lines = openFile.readlines()
        
        for line in lines:
            line = line.strip("\n\r")
            newLine = ""
            wordList = line.split(" ")
            
            if wordList[0] == "URL:":
                pass
            
            elif wordList[0] == "TITLE:":
                del(wordList[0])
                tempLine = " ".join(wordList)
                newLine = "".join(ch for ch in tempLine if ch not in exclude)
            
            elif wordList[0] == "TAGS:":
                del(wordList[0])
                newLine = " ".join(wordList)
                commaSplit = newLine.split(",")
                newLine = " ".join(commaSplit)
            
            elif wordList[0] == "AUTHOR:":
                del (wordList[0])
                pass

            else:
                tempLine = " ".join(wordList)
                newLine = "".join(ch for ch in tempLine if ch not in exclude)
            
            newLine = newLine.strip(" ")    
            #print newLine
            newLine = getOnlyNouns(newLine)
            writeFile.write(newLine + " ")

#This function takes only the noun 
def getOnlyNouns(line):
    newLine = []
    lineTags = nltk.pos_tag(line.strip("\n").split(" "))
    for word, tag in lineTags:
        if tag == "NN" or tag == "NNP" or tag == "NNS":
            #print word, tag
            newLine.append(word)
    
    return " ".join(newLine)
    
def groupByMonth(foldername, targetfoldername):
    newFolder = targetfoldername 
    if not os.path.exists(newFolder):
        os.makedirs(newFolder)
        
    for subdir, dirs, documents in os.walk(foldername):
        for filename in documents:
            if not os.path.exists(newFolder+filename[0:6]):
                os.makedirs(newFolder+filename[0:6])
            shutil.copy(subdir+"/"+filename, newFolder+filename[0:6]+"/")

def runStemmer():
    stemFiles("Cleaned_data/")
    #groupByMonth("Stemmed_data/", "Stemmed_Month_data/")

def runLemmatizer():
    lemmatizeFiles("Cleaned_data/")
    #groupByMonth("Lemmatized_data/", "Lemmatized_Month_data/")

   
def main():
    #cleanFiles("../nlp_project/techcrunch/")
    #runStemmer()
    runLemmatizer()

if __name__ == "__main__":
	main()
                
