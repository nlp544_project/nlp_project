'''
Author: Anvesha Sinha
Date: 04/16/2015

This script takes (document to topN topic mapping) file as the first parameter and (word to topic mapping) file as the second parameter. For topic to document mapping, a folder with files is created, where filenames are YYYYMM and each line in the file is the list of documents for that topic. For topic to word mapping, it creates a folder where files contain words related to a topic in each line starting from the first topic. The name of the file is formatted as YYYYMM.
'''

from collections import defaultdict
import sys
import re
import os

def TopicToDoc(filename, targetfile):
    topicDoc = {}
     
    with open(filename, "r") as openFile:
        openFile.next()
        for line in openFile:
            
            if "-" not in targetfile:
                line = re.sub(".*\d/","", line)
            else:
                line = re.sub(".*data/","", line)
            #Remove decimal values from the text
            line = re.sub("\s((\d)+\.(\d)+)","", line)
            line = re.sub("\t", " ", line)
            line = line.strip("\n ")
            #print line
            wordList = line.split(" ")
            #print wordList
            #print len(wordList)
            for j in range(1,len(wordList)):
                if int(wordList[j]) in topicDoc.keys():
                    topicDoc[int(wordList[j])].append(wordList[0])
                    topicDoc[int(wordList[j])] = list(set(topicDoc[int(wordList[j])]))
                    
                else:
                    topicDoc[int(wordList[j])] = []
                    topicDoc[int(wordList[j])].append(wordList[0])
                    
    
#    print topicDoc
#    with open("TopicToDoc.json", "w") as writeFile:
#        json.dump(topicDoc, writeFile, indent=4)
    
    PATH = "TopicToDoc/"
    if not os.path.exists(PATH):
        os.makedirs(PATH)
    
    #keyList = topicDoc.keys()    
    #keyList.sort()
    with open(PATH+targetfile,"w") as writeFile:
        for topic, doc in sorted(topicDoc.items()):
            #print topic
            writeFile.write(" ".join(doc) + "\n")
    
    print "TopicToDoc folder successfully written"

def TopicToWords(filename, targetfile):
    topicWord = defaultdict(int)
    
    with open(filename, "r") as openFile:
        openFile.next()
        openFile.next()
        openFile.next()
        for line in openFile:
            line = re.sub(".*(?=\s[a-z])","", line)
            line = line.strip("\n ")
            #print line
            wordList = line.split(" ")
            #print wordList
            if int(wordList[1]) in topicWord.keys():
                topicWord[int(wordList[1])].append(wordList[0])
                topicWord[int(wordList[1])] = list(set(topicWord[int(wordList[1])]))
                
            else:
                topicWord[int(wordList[1])] = []
                topicWord[int(wordList[1])].append(wordList[0])
                
    
    PATH = "TopicToWord/"
    if not os.path.exists(PATH):
        os.makedirs(PATH)
        
    with open(PATH+targetfile,"w") as writeFile:
        for topic, word in sorted(topicWord.items()):
            print topic
            writeFile.write(" ".join(word) + "\n")
    
    print "TopicToWord folder successfully written"
    
def main():
    #sys.argv[1]: doc-top3topic file
    #sys.argv[2]: word-topic file
    #sys.argv[3]: YYYYMM for the files
    
    TopicToDoc(sys.argv[1], sys.argv[3])
    TopicToWords(sys.argv[2], sys.argv[3])
    
if __name__ == "__main__":
	main()   
