'''
Author: Anvesha Sinha
Date: 04/20/2015

This script runs Topic Modelling (runTM() function) on folders containing articles, month wise. It also runs python command for running TMmappings.py (runTMmappings() function) and VecSim.py (runVecSim() function).
'''


import os
import tarfile
from os import listdir

def runTM(monthfolder):
    for subdirs, dirs, documents in os.walk(monthfolder):
        if not subdirs[-6:].isdigit():
            continue   
        #print subdirs, dirs
        TMfolder = "TMoutputs/"
        importedfile = TMfolder + "topic-input-" + subdirs[-6:] + ".mallet"
        wordToTopicFile = TMfolder + "word-topic-" + subdirs[-6:] + ".gz"
        docToTopicFile = TMfolder + "doc-topic-" + subdirs[-6:]
        print importedfile, wordToTopicFile
        
        if not os.path.exists(TMfolder):
		    os.makedirs(TMfolder)
        
        #Import files (month-wise) into Mallet
        os.system("mallet-2.0.7/bin/mallet import-dir --input "+ subdirs +" --output "+ importedfile +" \ --keep-sequence --remove-stopwords")
        
        #Get wordToTopic mapping & docToTopic mapping
        os.system("mallet-2.0.7/bin/mallet train-topics --input "+ importedfile +" \ --num-topics 200 --output-state "+ wordToTopicFile + " --output-doc-topics " + docToTopicFile +" --doc-topics-max 3")

def runTMAllMonths(monthfolder):
    TMfolder = "TMAllMonthsOutputs/"
    importedfile = TMfolder + "topic-input.mallet"
    wordToTopicFile = TMfolder + "word-topic.gz"
    docToTopicFile = TMfolder + "doc-topic"
    print importedfile, wordToTopicFile
    
    if not os.path.exists(TMfolder):
	    os.makedirs(TMfolder)
    
    #Import files (month-wise) into Mallet
    os.system("mallet-2.0.7/bin/mallet import-dir --input "+ monthfolder +" --output "+ importedfile +" \ --keep-sequence --remove-stopwords")
    
    #Get wordToTopic mapping & docToTopic mapping
    os.system("mallet-2.0.7/bin/mallet train-topics --input "+ importedfile +" \ --num-topics 200 --output-state "+ wordToTopicFile + " --output-doc-topics " + docToTopicFile +" --doc-topics-max 3")
                 
        
def runTMmappingsAllMonths(folder):
    print "running python TMmappings.py "+ folder +"doc-topic "+ folder + "word-topic 2010-15" 
    os.system("python TMmappings.py "+ folder +"doc-topic "+ folder + "word-topic 2010-15")

        
def main():
    #runTM("Lemmatized_Month_data/")
    #runTMmappings("TMoutputs/")
    
    #runTMAllMonths("Lemmatized_data/")
    runTMmappingsAllMonths("TMAllMonthsOutputs/")
    
    
if __name__ == "__main__":
    main()
