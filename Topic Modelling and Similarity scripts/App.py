import os
import integrated
from os import listdir

def runVecSim(tweetsFile, articleYear):
    #Reading only the most popular tweet
    pop_tweet = tweetsFile.readlines()[0].strip("\n")
    print "The most popular tweet is ", pop_tweet
    #print articleYear
    if articleYear == "2014-15":
        print "running python VecSim.py 2014-15/TopicToWord/ \"" + pop_tweet + "\" 2014-15/TopicToDoc/ " + "2014-15/Lemmatized_data/ " + str(articleYear)
        os.system("python VecSim.py 2014-15/TopicToWord/ \"" + pop_tweet + "\" 2014-15/TopicToDoc/ " + "2014-15/Lemmatized_data/ " + str(articleYear))
    elif articleYear == "2010-13":
        print "running python VecSim.py 2010-13/TopicToWord/ \"" + pop_tweet + "\" 2010-13/TopicToDoc/ " + "2010-13/Lemmatized_data/ " + str(articleYear)
        os.system("python VecSim.py 2010-13/TopicToWord/ \"" + pop_tweet + "\" 2010-13/TopicToDoc/ " + "2010-13/Lemmatized_data/ " + str(articleYear))
    else:
        print "Enter proper range"
    #else:
    #   print "running python VecSim.py 2014-15/TopicToWord/ \"" + pop_tweet + "\" 2010-15/TopicToDoc/ " + "2010-15/Lemmatized_data/ " + str(articleYear)
     #   os.system("python VecSim.py Month-wise/TopicToWord/ \"" + pop_tweet + "\" Month-wise/TopicToDoc/ " + "Month-wise/Month_data/" + str(articleYear)) + " " + str(articleYear))
    
        
def staticResults(tweetsfolder, articleYear):
    for filename in sorted(listdir(tweetsfolder)):
        openFile = open(tweetsfolder+filename,"r")
        runVecSim(openFile, articleYear)
 

def onlineTweet(articleYear):
    queryString = str(raw_input("Enter the products/companies you want to compare: "))
   
    #Extracting tweets online
    os.system("python3 integrated.py " + queryString)
    os.system("python extractNouns.py")
    
    #Read the tweets file
    openFile = open("list_nouns","r")
    runVecSim(openFile, articleYear)  
    
def main():
    string = "Menu\n1. Print the most popular articles for year and month\n2. Run system on-the-fly\n3. Exit\nEnter your choice: "
    userChoice = input(string)
    if userChoice == 1:
        articleYear = raw_input("Enter \n1.2010-13 for documents from 2010-13\n2.2014-15 for documents from 2014-15: ")
        staticResults("Tweets/",articleYear)
    elif userChoice == 2:
        articleYear = raw_input("Enter \n1.2010-13 for documents from 2010-13\n2.2014-15 for documents from 2014-15: ")
        onlineTweet(articleYear)
    elif userChoice == 3:
        exit()
    else:
        print ("Invalid choice")
        

if __name__ == "__main__":
	main()
