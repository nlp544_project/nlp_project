'''
Author: Anvesha Sinha
Date: 04/18/2015

This script finds out the similarity between a tweet and the topics. The topic which has the highest similarity is chosen and top5 documents from that topic are displayed. 
'''

from gensim import corpora, models, similarities
from collections import defaultdict
import codecs
import sys
from ProcessScrappedFiles import stemQuery, lemmatizeQuery
from os import listdir
import subprocess

#This function will create the vector representations of the words in each topic, for every month.
def createTopicVectors(topicWords):
    stopList = set("for a of the and to in".split())
   
    #print topicWords
    texts = [[word for word in topic.strip("\n").lower().split() if word not in stopList] for topic in topicWords]
    
    #Calculate the frequency of each word
    frequency = defaultdict(int)
    for text in texts:
        for token in text:
            frequency[token] += 1
    
    #texts = [[token for token in text if frequency[token] > 1] for text in texts]      
    
    #Give ID to each word
    id2wordDict = corpora.Dictionary(texts)
    #print "ID for each word ", id2wordDict.token2id
    
    #Create the vectors
    topicVec = [id2wordDict.doc2bow(text) for text in texts]
    #print topicVec
    
    #print "The Topic Vectors are ", topicVec     
    return topicVec, id2wordDict
        
def createTweetVector(tweet, id2wordDict):
    tweetVec = id2wordDict.doc2bow(tweet.lower().split())
    print "The tweet is: ", tweet
    print "The tweet vector is ", tweetVec
    return tweetVec

def tweetTopicSimilarity(topicVec, id2wordDict, tweetVec):

    #Define a 2-dimensional LSI space
    lsi = models.LsiModel(topicVec, id2word=id2wordDict, num_topics=200)
    
    #LSI for tweetVector
    tweet_lsi = lsi[tweetVec]
    #print tweet_lsi
    
    #LSI for topicVector
    index = similarities.MatrixSimilarity(lsi[topicVec])
    
    #Calculate similarity
    sims = index[tweet_lsi]
    sims = sorted(enumerate(sims), key=lambda item: -item[1])
    #print (sims)
    return sims

#This function uses cosine distance to get the first five relevant files from the topic.
def getTop5Documents(tweet, Topic, TopicToDocFolder, filename, articlesFolder):
    topic = Topic+1
    print "Topic chosen: ", topic
    
    #Get all the documents from the Topic chosen. DocWords is a list containing documents belonging to the topic line-wise.      
    with open(TopicToDocFolder+filename, "r") as openFile:
        lines = openFile.readlines()
        docList = lines[Topic].split(" ")
        
        docWords = []
        for document in docList:
            document = document.strip("\n")
            docFile = open(articlesFolder+document,"r")
            docWords.append(" ".join(docFile.readlines()))
    
    #Perform similarity of the tweet with each of the documents. Get the top 5 (or less) similar documents.  
    topicVec, id2wordDict = createTopicVectors(docWords)
    tweetVec = createTweetVector(tweet, id2wordDict)
    highestSimDoc = tweetTopicSimilarity(topicVec, id2wordDict, tweetVec)
    
    documents = []
    #print len(docWords)
    #print highestSimTopic
    numDoc = 5
    if len(highestSimDoc) < 5:
        numDoc = len(highestSimDoc)
    
    for i in range(numDoc):
        tempDict = {}
        DOC_ID = docList[highestSimDoc[i][0]].strip("\n")
        docFile = open("techcrunch/"+DOC_ID ,"r")
        tempDict["DOC_ID"] = DOC_ID
        tempDict["Cosine Similarity" ] = str(highestSimDoc[i][1])
        #tempDict["Keywords:"] = docWords[highestSimDoc[i][0]]
        URL = docFile.readline().replace("URL:","").strip(" \r\n")
        sentiment = subprocess.check_output(["python3", "get_article_sentiment.py", URL])
        tempDict["URL"] = URL
        tempDict["Sentiment"] = sentiment.strip("\n")
        documents.append(tempDict)
        
    #ADD SENTIMENT
    #ADD POPULARITY
    doc_num = 1             
    for document in documents:
        print doc_num, ". URL: ", document['URL']
        print "Cosine Similarity with the tweet: ", document['Cosine Similarity']
        print "Sentiment: ", document["Sentiment"]
        print
        doc_num += 1
        
def main():
    #sys.argv[1]: Folder containing topicToWord mapping
    #sys.argv[2]: tweet
    #sys.argv[3]: Folder containing topicToDoc mapping
    #sys.argv[4]: Folder containing all the articles
    #sys.argv[5]: YYYYMM
    
    #print sys.argv[5]
    openFile = codecs.open(sys.argv[1]+sys.argv[5], "r", encoding="utf-8", errors="ignore")
    topicWords = openFile.readlines()
    processedTweet = stemQuery(sys.argv[2])
    processedTweet = lemmatizeQuery(sys.argv[2])
    
    topicVec, id2wordDict = createTopicVectors(topicWords)
    tweetVec = createTweetVector(processedTweet, id2wordDict)
    highestSimTopic = tweetTopicSimilarity(topicVec, id2wordDict, tweetVec)[0][0]
    getTop5Documents(processedTweet, highestSimTopic, sys.argv[3], sys.argv[5], sys.argv[4])
    

if __name__ == "__main__":
    main()  
