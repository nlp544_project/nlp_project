import re
import codecs
import sys
import hashlib

dict1={}
dict2={}
dict3={}


def clean_tweet(text):

   text = re.sub(r'\w+:\/{2}[\d\w-]+(\.[\d\w-]+)*(?:(?:\/[^\s/]*))*', '', text)
   text = re.sub(r'@\w*','',text)
   #text= re.sub(r'[^\p{Latin}]', '', text)
   text=re.sub(r'\w*RT\w*','',text)
   text=text.lower()
   text=re.sub(r'\s+',' ',text)
   text1=re.sub(r'^\s+','',text)
   text=hashlib.md5(text1.encode('utf-8')).hexdigest()
   dict2[text]=text1
   return text
	   
def main():

   list_count_tweet=[]
   list_md5=[]
   myInput=codecs.open(sys.argv[1],"r",errors="ignore")
   i=0
   for text in myInput.readlines():
      
      fields = text.split("|||")
      if len(fields) != 15:
         continue
      tweet_text = fields[11]
      md5_cleaned_tweet = clean_tweet(tweet_text)
      if md5_cleaned_tweet not in list_md5:
         list_md5.append(md5_cleaned_tweet)
      
      if md5_cleaned_tweet in dict1:
         dict1[md5_cleaned_tweet]+=1
      else:
         dict1[md5_cleaned_tweet]=1
      dict1[md5_cleaned_tweet]=int(fields[7])
   for element in list_md5:
      list_md5=[]
      list_md5.append(dict1[element])
      list_md5.append(dict2[element])
      dict3[element]=list_md5
   out=open(sys.argv[2],"w")
   for key,value in dict3.items():
      out.write(str(value[0])+","+str(value[1]))
      out.write("\n")
      
      
if __name__=="__main__":
   main()
   



