import json
import re
import os
import string
import sys
import urllib.request
from bs4 import BeautifulSoup
from datetime import datetime, date, timedelta
import time

BASE_URL = "http://talkinterest.com:8088/?endpoints=sentiment&mode=document&language=auto&url=<TEST_URL>"

f_log = open("sentiment_log.txt","a")

def get_sentiment_value(tc_url):
    try:
        url = BASE_URL.replace("<TEST_URL>", tc_url)
        response = urllib.request.urlopen( url )
        content = response.read()
        json_dict = json.loads(content.decode(encoding='utf_8', errors='strict'))
        sentiment = json_dict["sentiment"]["polarity"]
        return sentiment
        #subjectivity = json_dict["sentiment"]["subjectivity"]
        #polarity_confidence = json_dict["sentiment"]["polarity_confidence"]
        #sub_conf = json_dict["sentiment"]["subjectivity_confidence"]
        #return (str(sentiment), str(polarity_confidence), str(subjectivity), str(sub_conf))
    except Exception as e:
        #f_log.write(e)
        f_log.write(tc_url + "\n")
        return ()
        
def main(url):
    value = get_sentiment_value(url.strip())
    #print (value)
    if value is not None:
        return value
    else:
        print ("Unable to retrieve Sentiment\n")
        return 0
        
if __name__ == "__main__":
    if len(sys.argv) < 2:
        print ("Please enter the URL of the article")
        exit()
    main(sys.argv[1])
