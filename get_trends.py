from bs4 import BeautifulSoup
from selenium import webdriver
import re
import sys
import urllib.request

import random
import time



BASE_URL = "https://www.google.com/trends/trendsReport?hl=en-US&q=<KEYWORD>&date=1/2010%2064m&cmpt=q&tz=&tz=&content=1"

def get_soup(url):
    try:
        response = urllib.request.urlopen( url )
    except urllib.error.HTTPError as e:
        print( "HTTPError with: ", url, e )
        return None
    page = response.read()
    soup = BeautifulSoup( page )
    return soup

def get_trend_change(soup):
    try:
        #print (soup)
        div = soup.find(id="TIMESERIES_GRAPH_0")
        div2 = div.find("script")
        text = div2.text
        re_expr = re.search('March 2015(.*)]]}', text)
        mar_apr_str = re_expr.group(1)
        print (mar_apr_str)
        re_ans_expr = re.search("\)\},(.*?),(.*?),(.*?),(.*?)\)\},(.*?),(.*?),(.*?),", mar_apr_str)
        mar,apr = re_ans_expr.group(3), re_ans_expr.group(7)
        change_pct = ((int(apr) - int(mar))/int(mar)) * 100
        return change_pct
    except:
        return 0


def main(keywords):
    mydriver = webdriver.Firefox()
    changes = []
    for kw in keywords:
        try:
            url = BASE_URL.replace("<KEYWORD>", kw)
            print(url)
            mydriver.get(url)
            content = mydriver.page_source
            soup = BeautifulSoup(content)
            if soup is None:
                continue
            changes.append(get_trend_change(soup))
            time.sleep(int(random.random()*60))
        except:
            changes.append(0)
    print(changes)
    total = 0
    for x in changes:
        total += x
    if len(changes) > 0:
        total = total/len(changes)
    print(total)
        

if __name__== "__main__":
    if len(sys.argv) < 2:
        print ("Provide space separated keywords as argument")
        exit()
    main(sys.argv[1:])
