import re
import codecs
import sys
import hashlib

myInput=codecs.open(sys.argv[1],"r",errors="ignore")
for text in myInput:
	text = re.sub(r'\w+:\/{2}[\d\w-]+(\.[\d\w-]+)*(?:(?:\/[^\s/]*))*', '', text)
	text = re.sub(r'@\w+','',text)
	#text= re.sub(r'[^\p{Latin}]', '', text)
	text=re.sub(r'\w*RT\w*','',text)
	text=text.lower()
	text=re.sub(r'\s+',' ',text)
	text=re.sub(r'^\s+','',text)
	text=hashlib.md5(text.encode('utf-8')).hexdigest()
	print (text)




