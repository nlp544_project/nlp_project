import sys
import os
import re
import codecs
import json
import nltk
import string
from nltk.corpus import stopwords



def main():
	stops = set(stopwords.words('english'))
	dictionary={}
	year=""
	list_words=[]
	start=0
	pronouns=["all","another","any","anybody","anyone","anything","i","its","it","itself","both","little","several","she","some","somebody","someone",
"something","that","their","theirs","them","themselves","these","they","this","those","many","me","mine","my","more","most","much","each",
"either","everybody","everyone","everything","few","he","her","hers","herself","him","himself","one another","whom","whoever","whomever","he","hers","her","herself","him","himself","his","you","your","yours","yourself","yourselves",
"other","others","our","ours","ourselves","we","what","whatever","who","whoever","whom","whomever","whose","whichever","which","whose"]

	conjunctions=["and","or","but","nor","so","for","yet","after","because","once","though","when","as long as","what","while","as if","even though","so that","until","whether","although","before","provided","till","whenever","as","since","unless","wherever"]
	for files in sorted(os.listdir(sys.argv[1])):

		prev_year=year

		#print(files)
		year=""
		count=0
		for char in files:
		
			if count<6:
				year+=char
			count+=1
		#print year
		if prev_year!=year and start==1:
			dictionary[prev_year]=list(set(list_words))
			list_words=[]
		start=1
		path=os.getcwd()+"/"+sys.argv[1]+"/"+files
		myInput=codecs.open(path,"r",encoding = "utf-8",errors="ignore")
		remove_punctuation_map = dict((ord(char), u" ") for char in string.punctuation)
		for line in myInput:

			line=line.strip("\n")
			line=line.translate(remove_punctuation_map)
			#print line
			stri=nltk.pos_tag(line.split(" "))
			#print stri
			

			for pair in stri:
				flag=0
				for x in pair:
					if flag==0:
						prev_x=x
					if flag==1:
						
						if x=="NNP":
							
							if prev_x.lower() not in stops:
								if prev_x.lower() not in conjunctions :
									if prev_x.lower() not in pronouns:
										list_words.append(prev_x)
					flag+=1
		#print list_words


	dictionary[year]=list(set(list_words))
	list_words=[]

	#print dictionary
	jsonData={}
	jsonData['mainDictionary']=dictionary

	with open(sys.argv[2],"w+") as out_file:
		json.dump(jsonData,out_file,indent=4)	
		               
if __name__=="__main__":
	main()
	
	
