from __future__ import absolute_import, print_function

import sys
import tweepy
import re
import codecs
import hashlib
import operator
import string
import time
import twitter
#from tweepy.auth import OAuthHandler
from string import ascii_lowercase as letters1

out=open("middle","w")

dict1={}
dict2={}
dict3={}
l1=[]

# == OAuth Authentication ==
#
# This mode of authentication is the new preferred way
# of authenticating with Twitter.

# The consumer keys can be found on your application's Details
# page located at https://dev.twitter.com/apps (under "OAuth settings")
consumer_key="BZp89qKRYHOrvemNZJOQZZsdn"
consumer_secret="gVLY2crGKCMMkbkJQWOhDl9oZVyP6ApNKEOCEnfDa3ebon1TKN"

# The access tokens can be found on your applications's Details
# page located at https://dev.twitter.com/apps (located
# under "Your access token")
access_token="3161950159-rzQaG6xmBloCJNy2oa8BYG5QnzVoTB5Td4jcZii"
access_token_secret="FmSzOEFHZqFOi6UujWEmmM5LKolXfzmfoQXt3ytQFxBvz"

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
#auth = twitter.Api(consumer_key, consumer_secret)
auth.secure = True
auth.set_access_token(access_token, access_token_secret)

api = tweepy.API(auth)

f_op = open("tweet_db4.txt", "a")
field_separator = "|||"
header_fields = ["author_name","author_screenname","created_at",
                "favorite_count", "favorited", "id", "in_reply_to_status_id",
                "retweet_count", "retweeted", "source", "source_url", "text",
                "truncated", "user_name", "user_screenname"]
                
 
def clean_tweet(text):

   text = re.sub(r'\w+:\/{2}[\d\w-]+(\.[\d\w-]+)*(?:(?:\/[^\s/]*))*', '', text)
   text = re.sub(r'@\w*','',text)
   #text= re.sub(r'[^\p{Latin}]', '', text)
   text=re.sub(r'\w*RT\w*','',text)
   text=text.lower()
   text=re.sub(r'\s+',' ',text)
   text1=re.sub(r'^\s+','',text)
   text=hashlib.md5(text1.encode('utf-8')).hexdigest()
   dict2[text]=text1
   return text

def popular_tweets(tweet_list):

   list_count_tweet=[]
   list_md5=[]
   #myInput=codecs.open(sys.argv[1],"r",errors="ignore")
   i=0
   for text in tweet_list:
      
      fields = text.split("|||")
      if len(fields) != 15:
         continue
      tweet_text = fields[11]
      md5_cleaned_tweet = clean_tweet(tweet_text)
      if md5_cleaned_tweet not in list_md5:
         list_md5.append(md5_cleaned_tweet)
      
      if md5_cleaned_tweet in dict1:
         dict1[md5_cleaned_tweet]+=1
      else:
         dict1[md5_cleaned_tweet]=1
      dict1[md5_cleaned_tweet]=int(fields[7])
   for element in list_md5:
      list_md5=[]
      list_md5.append(dict1[element])
      list_md5.append(dict2[element])
      dict3[element]=dict1[element]
   #dict4={}   
   #dict4[dict2[element]]=dict1[element]
   #dict4={1:2,2:3,4:0}
   sorted_dict3=sorted(dict3.items(),key=operator.itemgetter(1))
   #print (sorted_dict3)
   
   for elem in sorted_dict3[::-1]:
      l1.append(dict2[elem[0]])
      out.write (str(elem[1])+":"+dict2[elem[0]])
      out.write("\n")
   #print(l1) 
   #extract_nouns() 
      

def get_tweets(query):
    tweet_list = []
    start_id = None
    #count = 0
    for i in range(10):
        try:
            results = api.search(q=query, max_id=start_id, lang="en", count=200)
            if len(results) <= 0:
                continue
            #count += len(results)
            for tweet in results:
                tweet_fields = []
                if not tweet:
                    continue

                if tweet.author:
                    tweet_fields.append(str(tweet.author.name))
                    tweet_fields.append(str(tweet.author.screen_name))
                else:
                    tweet_fields.append("-1")
                    tweet_fields.append("-1")

                if tweet.created_at:
                    tweet_fields.append(tweet.created_at.strftime("%Y-%m-%d::%H:%M:%S"))
                else:
                    tweet_fields.append("-1")

                if tweet.favorite_count is not None and  tweet.favorite_count >= 0:
                    tweet_fields.append(str(tweet.favorite_count))
                else:
                    tweet_fields.append("-1")
                    
                if tweet.favorited:
                    tweet_fields.append(str(tweet.favorited))
                else:
                    tweet_fields.append("-1")
                    
                if tweet.id:
                    tweet_fields.append(str(tweet.id))
                else:
                    tweet_fields.append("-1")
                    
                if tweet.in_reply_to_status_id is not None:
                    tweet_fields.append(str(tweet.in_reply_to_status_id))
                else:
                    tweet_fields.append("-1")
                    
                if tweet.retweet_count is not None:
                    tweet_fields.append(str(tweet.retweet_count))
                else:
                    tweet_fields.append("-1")

                if tweet.retweeted is not None:
                    tweet_fields.append(str(tweet.retweeted))
                else:
                    tweet_fields.append("-1")

                if tweet.source is not None:
                    tweet_fields.append(str(tweet.source))
                else:
                    tweet_fields.append("-1")

                if tweet.source_url is not None:
                    tweet_fields.append(str(tweet.source_url))
                else:
                    tweet_fields.append("-1")

                if tweet.text is not None:
                    tweet_fields.append(str(tweet.text.replace("\n", "")))
                else:
                    tweet_fields.append("-1")

                if tweet.truncated is not None:
                    tweet_fields.append(str(tweet.truncated))
                else:
                    tweet_fields.append("-1")

                if tweet.user:
                    tweet_fields.append(str(tweet.user.name))
                else:
                    tweet_fields.append("-1")
                    
                if tweet.user:
                    tweet_fields.append(str(tweet.user.screen_name))
                else:
                    tweet_fields.append("-1")
                    
                # Write to file
                tweet_list.append(field_separator.join(tweet_fields))
                
                if start_id is None:
                    start_id = tweet.id
                if start_id > tweet.id:
                    start_id = tweet.id

        except:
            print ("Exception while getting the tweets at start_id: ", start_id)
            print ("Returning with tweet count of ", len(tweet_list))
            break

    popular_tweets(tweet_list)


def main(keywords):
    query_str = " OR ".join(keywords)
    tweet_list = get_tweets(query_str)
    #print (tweet_list[:5])
    #print (len(tweet_list))


if __name__ == "__main__":
   if len(sys.argv) < 2:
        print ("Please enter keywords separated by space")
        exit()
   main(sys.argv[1:])


	   


