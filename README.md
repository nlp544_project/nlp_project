### TRENDICATOR: PREDICTING TRENDS USING SOCIAL MEDIA (Team 61)

This repository contains the scripts required for the final project of CSCI 544.

**Data collection & extraction scripts:
**

* **techcrunch_scraper.py:** Script to scrape the tech crunch website. This script uses python3 version of BeautifulSoup4 (bs4), use the following commands to install bs4. The date range for which the files to be downloaded can be specified inside the script where the execution starts. The files scraped from Techcrunch would be saved inside the "techcrunch_files" folder.
        
        sudo apt-get install python3-setuptools
        sudo easy_install3 pip
        sudo pip3 install beautifulsoup4

        python3 techcrunch_scraper.py 



* keyWordExtract.py
* **get_tweets.py:** This script takes the keywords as arguments and gets the tweets using "tweepy" API from Twitter and saves it to a file. This script can run continuously without throwing an exception when the tweet limit has been reached where it sleeps for a certain time period and tries to get the tweets again. Should be used cautiously and customized according to the requirements.
* **tweet_clean.py:**
* **extractTweets_includesSorting.py:**
* **extractTweets_version1.py:**

**Topic Modelling and Similarity scripts:
**

* ProcessScrappedFiles.py : This script processes the scrapped articles from Techcrunch and cleans them so that the files can be fed into Mallet.
          
          python ProcessScrappedFiles.py


* SystemCalls.py : This script runs Topic Modelling (runTM() function) on folders containing articles, month wise. It also runs python command for running TMmappings.py (runTMmappings() function) and VecSim.py (runVecSim() function).
          
          python SystemCalls.py 


* TMmappings.py : This script takes (document to topN topic mapping) file as the first parameter and (word to topic mapping) file as the second parameter. For topic to document mapping, a folder with files is created, where filenames are YYYYMM and each line in the file is the list of documents for that topic. For topic to word mapping, it creates a folder where files contain words related to a topic in each line starting from the first topic. The name of the file is formatted as YYYYMM.
          
          python TMmappings.py <doc-top3topic file> <word-topic file> <YYYYMM for the files>


* VecSim.py : This script finds out the similarity between a tweet and the topics. The topic which has the highest similarity is chosen and top5 documents from that topic are displayed. 
          
          python VecSim.py <Folder containing topicToWord mapping> <tweet> <Folder containing topicToDoc mapping> <Folder containing all the articles> <YYYYMM>


* App.py : This is the integrated script which includes both online and offline versions. It uses integrated.py and extractNouns.py scripts internally.
          
          python App.py

**Sentiment calculation and trends evaluation scripts:
**

* **get_sentiment.py:** This script is used to calculate the sentiment of the Techcrunch article. This script reads the urls of the articles from a file, computes its sentiment and write to a file along with the confidence score.
* **get_article_sentiment.py:** This script worls similar to the above script except that it takes the url as a command line argument.
* **get_trends.py:** This script calculates the average of % of change in the number of searches to previous month for a given set of keywords passed as command line args. This script makes use of selenium webdriver as the script calls to google-trends website are blocked. This should be used cautiously as Google blocks if you continuously call the trends website.
* **tf_idf.py:** This script gets the related articles for a given set of keywords using TF-IDF concept. This script is used to test how good the suggested articles from Topic Modelling is, compared to the suggested articles using tf-idf criterion.