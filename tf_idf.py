"""
Add your documents as two-element lists `[docname, [list_of_words_in_the_document]]` with `addDocument(docname, list_of_words)`. Get a list of all the `[docname, similarity_score]` pairs relative to a document by calling `similarities([list_of_words])`.
"""
from os import listdir
from os.path import isfile, join, basename
import sys
import os

class tfidf:
  def __init__(self):
    self.weighted = False
    self.documents = []
    self.corpus_dict = {}

  def addDocument(self, doc_name, list_of_words):
    # building a dictionary
    doc_dict = {}
    for w in list_of_words:
      doc_dict[w] = doc_dict.get(w, 0.) + 1.0
      self.corpus_dict[w] = self.corpus_dict.get(w, 0.0) + 1.0

    # normalizing the dictionary
    length = float(len(list_of_words))
    for k in doc_dict:
      doc_dict[k] = doc_dict[k] / length

    # add the normalized document to the corpus
    self.documents.append([doc_name, doc_dict])

  def similarities(self, list_of_words):
    """Returns a list of all the [docname, similarity_score] pairs relative to a list of words."""

    # building the query dictionary
    query_dict = {}
    for w in list_of_words:
      query_dict[w] = query_dict.get(w, 0.0) + 1.0

    # normalizing the query
    length = float(len(list_of_words))
    for k in query_dict:
      query_dict[k] = query_dict[k] / length

    # computing the list of similarities
    sims = []
    for doc in self.documents:
      score = 0.0
      doc_dict = doc[1]
      for k in query_dict:
        if k in doc_dict:
          score += (query_dict[k] / self.corpus_dict[k]) + (doc_dict[k] / self.corpus_dict[k])
      sims.append([doc[0], score])

    return sims

def get_file_list(data_dir):
    try:
        file_list = [data_dir + "/" +f for f in listdir(data_dir) if isfile(join(data_dir, f))]
        #file_list.sort()
        print ("No. of files in training dir = ", len(file_list))
        return file_list
    except FileNotFoundError as e:
        print ("Invalid directory path mentioned")
        exit()

def main(data_dir):
    file_list = get_file_list(data_dir)
    table = tfidf()
    for f in file_list:
        file_name = basename(f)
        fp = open(f, "r", errors="ignore")
        url = fp.readline().strip().replace("URL: ", "")
        # Skip 3 lines
        fp.readline()
        fp.readline()
        fp.readline()
        content = ""
        for l in fp.readlines():
            content +=  " " + l.strip()
        words = content.split(" ")
        words = [w for w in words if w is not None]
        table.addDocument(file_name + "|" + url, words)
    keywords = ["google", "launch","wireless", "cellular", "service"]
    results = table.similarities(keywords)
    for r in results:
        print (str(r[0])+"#"+str(r[1]))


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Please enter dir as argument")
        exit()
    main(sys.argv[1])
