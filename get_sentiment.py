import json
import re
import os
import string
import urllib.request
from bs4 import BeautifulSoup
from datetime import datetime, date, timedelta
import time

BASE_URL = "http://talkinterest.com:8088/?endpoints=sentiment&mode=document&language=auto&url=<TEST_URL>"

f_log = open("sentiment_log.txt","a")

def get_sentiment_value(tc_url):
    try:
        url = BASE_URL.replace("<TEST_URL>", tc_url)
        response = urllib.request.urlopen( url )
        content = response.read()
        json_dict = json.loads(content.decode(encoding='utf_8', errors='strict'))
        sentiment = json_dict["sentiment"]["polarity"]
        subjectivity = json_dict["sentiment"]["subjectivity"]
        polarity_confidence = json_dict["sentiment"]["polarity_confidence"]
        sub_conf = json_dict["sentiment"]["subjectivity_confidence"]
        return (str(sentiment), str(polarity_confidence), str(subjectivity), str(sub_conf))
    except Exception as e:
        #f_log.write(e)
        f_log.write(tc_url + "\n")
        return ()
        
def main(ip_file):
    f_in = open(ip_file, "r")
    f_out = open("sentiment_values.txt", "a")
    for url in f_in.readlines():
        value_list = get_sentiment_value(url.strip())
        if value_list and len(value_list) > 0:
            f_out.write(url.strip() + "|" + "|".join(value_list) + "\n")
        else:
            print ("Sleeping for 50 mins\n")
            time.sleep(2400)
    f_in.close()
    f_out.close()
        
if __name__ == "__main__":
    main("urls.txt")
